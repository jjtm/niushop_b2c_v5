### NiuShop开源商城V5版【厚积薄发，重拾信心！！！】

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220801163902.png)

### 公司介绍

  杭州牛之云科技有限公司成立于2016年，是一家从事移动互联网，电商软件为主导的技术研发型企业。公司总部位于上海，研发中心设立于锦绣龙城太原市，目前有团队成员50多人，产品研发实力雄厚。拥有NIUSHOP开源商城，NIUCLOUD开源小程序应用市场，牛客云商企业级SAAS电商服务平台等产品。业务遍及全国26个省市，在北京、广州、上海、深圳等地区拥有多家合作企业。我们始终保持专业、专注、专一的原则，旨在为用户提供最好用的全功能型电商软件产品，是您转型新零售，新媒体，掘金千亿电商市场的首选。

### 产品介绍 

 **快速搭建专属店铺，迅速展开线上业务** <br/>

默默耕耘，是为了在明天硕果累累；沉淀积累，是为了在未来厚积薄发！！自Niushop单商户V4于2020年上线以来，历时两年时间的迭代更新，应广大用户的要求，Niushop单商户V5时代终于来啦~ V5是一个全新的开始，升级重构多门店、收银一体化、软硬件物联、商品线上线下营销完全打通；前后端代码重组优化80%以上；
更加强大的DIY自定义装修；完全内置消息队列、Redis缓存服务，是大型商城运营的首选，抓紧下载体验起来吧~

### V5商城特色
强大的营销功能模块，丰富的行业模板和装修组件，快速搭建最适合自己的电商平台，轻松获客、裂变。开启电商运营之路。

1. &nbsp;<img src="https://images.gitee.com/uploads/images/2020/0724/121556_a96bd648_6569472.png"/>&nbsp;&nbsp;ThinkPhp6 + LayUi + ElementUi,学习维护成本低<br/>
2. &nbsp;<img src="https://images.gitee.com/uploads/images/2020/0724/121615_f801f981_6569472.png"/>&nbsp;&nbsp;前端由UNI-APP框架编写，支持多端，易于维护<br/>
3. &nbsp;<img src="https://images.gitee.com/uploads/images/2020/0724/121635_e51987c4_6569472.png"/>&nbsp;&nbsp;钩子 + 插件，组件化开发，可复用，开发便捷<br/>
4. &nbsp;<img src="https://images.gitee.com/uploads/images/2020/0724/121645_df103f55_6569472.png"/>&nbsp;&nbsp;标准API接口，前后端分离，二次开发更方便<br/>
5. &nbsp;<img src="https://images.gitee.com/uploads/images/2020/0724/121635_e51987c4_6569472.png"/>&nbsp;&nbsp;内置消息队列，全面支持redis缓存机制，支持大数据、高并发、大流量<br/>
6. &nbsp;<img src="https://images.gitee.com/uploads/images/2020/0724/121708_74c55984_6569472.png"/>&nbsp;&nbsp;代码全部开源，方便企业扩展自身业务需求

### 技术亮点

    1.框架采用全新thinkphp6+事件开发设计+layui+uniapp进行设计，代码完全重构，支持百万级！

    2.前端以layui + uniapp模块化开发；

    3.数据导出采用phpExcel,使数据更加直观，更方便于管理统计；

    4.插件钩子机制，功能模块独立，更有助于二次开发；

    5.后台采用ECharts,直观体现关系数据可视化的图，支持图与图之间的混搭。实现完善的数据统计和分析；

    6.完善的财务管理模块，账目的收入与支出直观明了，每一笔金额的走向清晰可见；
    
    6.EasyWeChat部署微信开发,微信接入更加快捷,简单；

    7.内置强大灵活的权限管理体系，有利于专人专项运营；

    8.内置组合数据统计,系统配置,管理碎片化数据统计；

    9.客户端完善的交互效果和动画，提升用户端视觉体验；

    10.可以完美对接公众号和小程序,并且数据同步,实现真正意义上的一端开发，多端使用；

    11.内置客服系统,可以对接企微客服、腾讯客服、小程序客服以及Niushop客服,客服在线实时聊天；

    12.高频数据redis缓存,数据库读写分离，很大程度减轻服务器压力，提升访问速度；

    13.后台设置菜单中可以一键数据备份和恢复，完全傻瓜式操作就可以轻松升级备份；

    14.在线一键升级，轻松跨越到最新版本；

    15.标准Api接口、前后端分离，二次开发更方便快捷;

    16.支持数据库结构、数据、模板在线缓存清除，提升用户体验；

    17.可视化DIY店铺装修，方便、快捷、直观，可以随心所欲装扮自己的店铺；

    18.无缝事件机制行为扩展更方便，方便二次开发；

    19.支持队列降低流量高峰，解除代码耦合性，高可用性;

    20.在线一键安装部署，自动检测系统环境一键安装，省时省力快捷部署；


### 系统功能
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E7%B3%BB%E7%BB%9F%E5%8A%9F%E8%83%BD%E7%B3%BB%E7%BB%9F%E5%8A%9F%E8%83%BD2.png)

### 看到这里，各位小主是不是都有点迫不及待想要一睹v5的风采了呢？好的，下面就部分更新功能给大家做个介绍吧~~:point_down:
#### 先来一睹前端模板的风采吧
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF1%E5%89%8D%E7%AB%AF1.jpg)
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF2%E5%89%8D%E7%AB%AF2.jpg)
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF3%E5%89%8D%E7%AB%AF3.jpg)
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF4%E5%89%8D%E7%AB%AF4.jpg)
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF5%E5%89%8D%E7%AB%AF5.jpg)
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF6%E5%89%8D%E7%AB%AF6.jpg)
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%89%8D%E7%AB%AF7%E5%89%8D%E7%AB%AF7.jpg)
#### 这样高大尚的界面是不是都有点迫不及待想要体验一把了呢，别着急，接着往下看，一大波优化及新增内容来袭~~
#### 新增登录引导页

* 本次升级引入了引导页，引导页相当于默认网页，用户安装好默认软件后，默认不会再打开后台页面，而会进入引导页。引导页默认包含网站后台、目的为避免客户输入域名跳转管理端的尴尬，或者在没有安装配置pc端时，因没有页面而报错。并且在默认引导页会显示备案号，在ICP审查时相当于默认主页。如果您不需要展示该默认页，也可删除或自行修改。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E7%99%BB%E5%BD%95%E5%BC%95%E5%AF%BC%E9%A1%B5%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220801174939.jpg)

#### 改版登录页
* V5版本重新设计了登录页，突出体现了智慧多门店、收银的重要程度及展示。本次时间仓促，没有实质性的升级门店，收银功能。继V5版本发布之后，NIUSHOP团队会在V5.1版本升级时，重构多门店、收银台功能。届时。门店核销、虚拟商品核销、会员开卡办、商品线上线下销售将完全打通，收银一体化，软硬件物联。V5.1版本会全部整合上线，静待佳音。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E6%94%B9%E7%89%88%E7%99%BB%E5%BD%95%E9%A1%B5%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220801174950.jpg)

#### 升级后台管理页面
* V5整体的后台管理界面样式，只保留一种风格，采用“冰灰+冰黑+科技蓝”色调搭配，更加体现了V5版本的科技感。V5的本次版本更新，代码整合、重构、去冗、文件命名规范化、结构调整等，整体达80%以上。而且本次更新各版本都内置redis缓存、消息队列、高并发、只需轻松配置即可拥有V5带来的全新感觉。NIUSHOP团队本次升级经过了长时间的深度设计及研发。在精度、深度、厚度等方面都做足了功夫，同时真心希望V5能在您的创业之路上助您一臂之力！

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86%E9%A1%B5%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220801180241.png)

#### 重构首页DIY功能
* V5升级最大的就是首页DIY功能的重构。前端界面就像是初次相识的陌生人，前端界面的好坏直接决定产品在用户心中的第一印象是否深刻，优秀的界面会让人一见钟情。所以V5的本次升级对以前模板设计进行了全新的颠覆设置，代码重构率80%以上，每一个DIY组件都进行了全新的参数和样式设计，V5版本独创图标合成功能，让您不懂美工、不会设计，也一样可以做出漂亮大气的时尚商城。强大的功能真的是三天三夜都说不尽~话不在多在于精，赶紧下载深入体验吧。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E9%A6%96%E9%A1%B5diy1QQ%E5%9B%BE%E7%89%8720220801182902.png)

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E9%A6%96%E9%A1%B5diy2QQ%E5%9B%BE%E7%89%8720220801183358.png)

#### 优化店铺模块
* 新增文章管理模块，应用户的要求，V5可以在管理端编辑发布文章了，前端可以装修文章专有区域模块。
* 重写分类页面，简化代码结构，用户可以选择分类的页面样式，并进行细节设置。本次升级也对左侧一级分类导航自动跟随右侧二三级分类或商品滚动进行行了优化，用户可以在滚动右侧分类或者查看右侧商品的同时左侧以及分类也会跟随滚动选中。
* 会员中心自定义页面也进行了全新的设计，对基础的代码稳定和简化做了核心优化，后续版本中还会继续增强设置选项。
* 底部导航可以使用图片或者自主合成图标，即使不懂美工，不会设计，也同样可以自动化生成高级图标元素。
* 商城的风格增加了丁香紫、明艳黄、炫酷黑三种色调，并对所有的模板色调进行了细节的组合样式优化。
* 继V5发布之后，pc端模板会进行深入优化，敬请期待吧~

#### 优化商品模块
* 商品主要改动就是把商品采集、商品导入分拆到商品工具菜单下，简化了商品添加页面，对于不需要这些功能的用户，在商品添加编辑页看起来更加清爽。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%95%86%E5%93%81%E9%87%87%E9%9B%86%E3%80%81%E5%AF%BC%E5%85%A5%E5%95%86%E5%93%81%E5%AF%BC%E5%85%A5%E3%80%81%E9%87%87%E9%9B%86%E8%8F%9C%E5%8D%95%E5%BD%92%E9%9B%86.png)

* 添加了商品品牌模块，同时在自定义DIY首页中可以对其进行展示和维护。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%95%86%E5%93%81%E5%93%81%E7%89%8C1%E5%95%86%E5%93%81%E5%93%81%E7%89%8C%E7%AE%A1%E7%90%86.png)

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%95%86%E5%93%81%E5%93%81%E7%89%8C2%E5%95%86%E5%93%81%E5%93%81%E7%89%8C%E8%A3%85%E4%BF%AE.png)

* 优化商品服务功能，把服务内容的图标由图片上传修改为图标合成和图标上传两部分，可以更简单的处理商品服务的图标展示。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%95%86%E5%93%81%E6%9C%8D%E5%8A%A1%E5%95%86%E5%93%81%E6%9C%8D%E5%8A%A1%E5%90%88%E6%88%90%E5%9B%BE%E6%A0%87.png)

* 商品列表优化了会员价图标，简化了其样式，原先隐藏到更多菜单中的编辑功能，现在直接展示在快捷操作中。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%95%86%E5%93%81%E4%BC%9A%E5%91%98%E4%BB%B7%E6%A0%87%E7%AD%BE%E4%BC%9A%E5%91%98%E4%BB%B7%E5%9B%BE%E6%A0%87%E6%A0%B7%E5%BC%8F.png)

* 优化了添加和编辑商品功能，价格和库存的处理单独放在了TAB页面，高级设置下的虚拟销量、单位、商品服务等参数分别转移到基础设置和价格库存选项卡下。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%95%86%E5%93%81%E5%BA%93%E5%AD%98%E3%80%81%E9%94%80%E9%87%8F%E7%AD%89%E9%AB%98%E7%BA%A7%E8%AE%BE%E7%BD%AE%E5%BA%93%E5%AD%98%E3%80%81%E5%88%92%E7%BA%BF%E4%BB%B7%E3%80%81%E9%94%80%E9%87%8F%E7%AD%89%E9%AB%98%E7%BA%A7%E8%AE%BE%E7%BD%AE.png)

* 虚拟商品增加自动收发货以及手动收发货的设置

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%99%9A%E6%8B%9F%E5%95%86%E5%93%81%E6%89%8B%E5%8A%A8%E3%80%81%E8%87%AA%E5%8A%A8%E5%8F%91%E8%B4%A7%E8%99%9A%E6%8B%9F%E5%95%86%E5%93%81%E6%89%8B%E5%8A%A8%E8%87%AA%E5%8A%A8%E6%94%B6%E5%8F%91%E8%B4%A7.png)

* 图片上传功能是v5的重量级优化，对于原始图，一律采用原始图切割算法，不留白，不失真；图片太多一片混乱怎么办？Niushop V5版新增了图片二级分类，可对图片分层管理；同时引入了动态gif的上传处理，可支持动图啦！！！这波操作着实让人心动~

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E7%9B%B8%E5%86%8C%E4%BA%8C%E7%BA%A7%E5%88%86%E7%B1%BB%E3%80%81gif%E5%8A%A8%E5%9B%BE%E5%9B%BE%E7%89%87%E6%94%AF%E6%8C%81%E4%BA%8C%E7%BA%A7%E5%88%86%E7%B1%BB%EF%BC%8C%E6%94%AF%E6%8C%81%E5%8A%A8%E5%9B%BE.png)

* 优化了商品详情图片展示，默认展示商品图，当选择规格时展示规格图。
* 商品添加、编辑的时候，分组表头位置锁定，方便快速切换编辑其他设置项。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%A1%A8%E5%A4%B4%E9%94%81%E5%AE%9A%E6%B7%BB%E5%8A%A0%E7%BC%96%E8%BE%91%E5%95%86%E5%93%81%E8%A1%A8%E5%A4%B4%E9%94%81%E5%AE%9A.png)

#### 优化订单模块
* 订单管理模块主要把各类型订单处理进行了重新的归组，商品订单、充值订单、会员卡订单、礼品卡订单、积分兑换订单、收银订单、退款维权等都会各自显示，可以更清晰的快速通过订单列表查询您需要查询的数据。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%AE%A2%E5%8D%95%E7%B1%BB%E5%9E%8B%E6%95%B4%E5%90%88%E8%AE%A2%E5%8D%95%E7%B1%BB%E5%9E%8B%E5%88%86%E7%B1%BB%E5%BD%92%E9%9B%86.png)

* 收银台订单单独提出来，统一归为收银台订单去管理，V5.1之后会对收银台功能深入优化。
* 优化订单退款，商家管理端不仅可以一键退款，同时可对单独订单项进行主动退款，适用了更广泛的商家运营。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%AE%A2%E5%8D%95%E9%A1%B9%E5%8D%95%E7%8B%AC%E9%80%80%E6%AC%BE%E8%AE%A2%E5%8D%95%E9%A1%B9%E5%8D%95%E7%8B%AC%E9%80%80%E6%AC%BE.png)

* 库存管理采用Redis高并发问题处理解决方案。
* 增加商家收发货地址的管理。
* 运费模板增加了免邮配送区域。
* 打印发货单扩大打印范围，取消针对订单状态可打印。
* 强化小票打印功能，针对不同的配送方式，自提、门店、充值、消费等可以单独进行打印格式和数据设置。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E5%B0%8F%E7%A5%A8%E6%89%93%E5%8D%B0%E9%92%88%E5%AF%B9%E4%B8%8D%E5%90%8C%E9%85%8D%E9%80%81%E6%96%B9%E5%BC%8F%E5%B0%8F%E7%A5%A8%E6%89%93%E5%8D%B0%E9%92%88%E5%AF%B9%E4%B8%8D%E5%90%8C%E9%85%8D%E9%80%81%E6%96%B9%E5%BC%8F.png)

#### 重组会员模块
* 会员模块重新进行了整合，按照会员管理、等级权益、会员处置、会员积分进行了功能分组，增强了会员规则的功能体现，可以更简单的进行会员的管理和运营。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E4%BC%9A%E5%91%98%E6%A8%A1%E5%9D%97%E5%8A%9F%E8%83%BD%E6%95%B4%E5%90%88%E4%BC%9A%E5%91%98%E8%A7%84%E5%88%99%E5%8A%9F%E8%83%BD%E5%88%86%E7%BB%84%E6%95%B4%E5%90%88.png)

* 重新设计了会员概况样式，细化了会员数据的展示。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E4%BC%9A%E5%91%98%E6%A6%82%E5%86%B5%E6%95%B0%E6%8D%AE%E5%B1%95%E7%A4%BA%E6%A0%B7%E5%BC%8F%E4%BC%9A%E5%91%98%E6%A6%82%E5%86%B5%E6%95%B0%E6%8D%AE%E5%B1%95%E7%A4%BA%E6%A0%B7%E5%BC%8F.png)

* 会员列表增加了会员来源的显示，便于商家分析运营投放目标。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E4%BC%9A%E5%91%98%E5%88%97%E8%A1%A8%E5%A2%9E%E5%8A%A0%E6%9D%A5%E6%BA%90%E6%B8%A0%E9%81%93%E4%BC%9A%E5%91%98%E5%88%97%E8%A1%A8%E5%A2%9E%E5%8A%A0%E6%9D%A5%E6%BA%90%E6%B8%A0%E9%81%93.png)

* 新增批量发放优惠券，每张优惠券可设置发放数量，领取几天后有效等。
* 会员积分、优惠券、余额、成长值等，增加流水展示，方便商家能够准确查询核对。

#### 优化影响模块
* 营销中心和应用工具都合并整合到营销模块菜单下，可以通过营销中心和应用工具添加快捷方式到模块菜单，便于快速使用常用功能模块。
* 新增营销概况模块，可在营销概况中快速查看正在进行的活动、将要结束的活动以及各活动基础配置等。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%90%A5%E9%94%80%E6%A6%82%E5%86%B5%E8%90%A5%E9%94%80%E6%A6%82%E5%86%B5.png)

* 新增各营销活动选择商品可按照商品分组选择。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%90%A5%E9%94%80%E6%B4%BB%E5%8A%A8%E9%80%89%E6%8B%A9%E5%95%86%E5%93%81%E6%8C%89%E5%88%86%E7%BB%84%E6%9F%A5%E8%AF%A2%E8%90%A5%E9%94%80%E6%B4%BB%E5%8A%A8%E9%80%89%E6%8B%A9%E5%95%86%E5%93%81%E6%8C%89%E5%88%86%E7%BB%84%E6%9F%A5%E8%AF%A2.png)

#### 迁移门店模块
* 门店模块在本次升级中，转移到了设置菜单项中，考虑到门店列表属于基础功能，却又非人人需要的功能模块，放到顶级菜单牵强而又以小博大，还是静待V5.1给您带来的智慧多门店全新重构吧~

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E9%97%A8%E5%BA%97%E8%8F%9C%E5%8D%95%E8%BF%81%E7%A7%BB%E9%97%A8%E5%BA%97%E8%8F%9C%E5%8D%95%E8%BF%81%E7%A7%BB.png)

#### 优化财务模块
* 不确切的讲,目前绝大多数商城系统的财务数据板块华而不实，为了存在而摆设，财务数据毫无关联，单一而又无价值。V5的升级，对财务报表进行了重新设计，商家的收入、支出等各项数据明细来源直接、清晰、明了。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E8%B4%A2%E5%8A%A1%E7%BB%9F%E8%AE%A1%E6%A8%A1%E5%9D%97%E8%B4%A2%E5%8A%A1%E7%BB%9F%E8%AE%A1%E6%A8%A1%E5%9D%97.png)

#### 更改统计模块
* 之前的统计菜单改为了数据，增加了数据概况、会员数据、会员卡数据的统计和展示，界面也进行了全新设计，后续版本升级中会继续优化数据功能模块。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E7%BB%9F%E8%AE%A1%E6%A8%A1%E5%9D%97%E7%BB%9F%E8%AE%A1%E6%A8%A1%E5%9D%97.png)

#### 迁移渠道模块
* V5版本去掉了渠道顶级菜单，将其迁移到了设置菜单项中，微信新api接口升级。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E6%B8%A0%E9%81%93%E8%8F%9C%E5%8D%95%E8%BF%81%E7%A7%BB%E6%B8%A0%E9%81%93%E8%8F%9C%E5%8D%95%E8%BF%81%E7%A7%BB.png)

#### 迁移授权模块
* 授权模块迁移到设置菜单项中

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E6%8E%88%E6%9D%83%E8%BF%81%E7%A7%BB%E6%8E%88%E6%9D%83%E8%BF%81%E7%A7%BB.png)

#### 重组设置模块

* V4的版权信息迁移到V5系统维护下。
* 新增门店管理菜单。
* 权限管理改为员工管理。
* 会员设置改为注册登录菜单项。
* 客服迁移到设置菜单下。
* 短信管理改为短信中心。
* 合并了杂项功能菜单到设置菜单项中，上传设置、云上传，迁移到其他设置菜单项。
* 增加方形logo设置。

![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E6%96%B9%E5%BD%A2logo%E8%AE%BE%E7%BD%AE%E6%96%B9%E5%BD%A2logo%E8%AE%BE%E7%BD%AE.png)

#### 其他优化项
* 商家位置、配送区域、地区全部改成了腾讯地图，不再使用高德地图。
* 发送微信模板消息、系统任务、定时触发、并发库存等都加入了消息队列执行。
* 商品详情加入了最近付款订单提示。
* 个人中心底部增加猜你喜欢模块
#### 好啦！话不多说，相信你早已跃跃欲试了，那就行动起来赶紧下载体验吧！
###  内测演示二维码
![输入图片说明](https://gitee.com/niushop_team/niushop_b2c_v5_stand/raw/master/image/%E6%BC%94%E7%A4%BA%E4%BA%8C%E7%BB%B4%E7%A0%81%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220801173711.png)

### 内测演示站后台[<a href='https://v5.niuteam.cn/shop' target="_blank"> 查看 </a>]       
<a href='https://v5.niuteam.cn/shop' target="_blank">https://v5.niuteam.cn/shop</a>  账号：test  密码：niushoptest

### 操作指南
 [Niushop开源商城单商户V5使用手册](https://www.kancloud.cn/niucloud/niushop_b2c_v4/1842076)
 | [论坛地址](https://bbs.niushop.com)
 | [官网地址](https://www.niushop.com)

### 推荐阿里云服务器配置

![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/095424_ac477fe3_6569472.png "fuwuqi.png")

### 开源版使用须知

1.仅允许用于个人学习研究使用;

2.开源版不建议商用，如果商用必须保留版权信息，望自觉遵守;

3.禁止将本开源的代码和资源进行任何形式任何名义的出售，否则产生的一切任何后果责任由侵权者自负。

4.本基础版本后台源码全部开源，小程序为uniapp编译版，如需小程序源码，请购买授权。

### Niushop官方群
 Niushop商城qq开发群1:<a href="https://jq.qq.com/?_wv=1027&k=VrVzi1FI" target="_blank"><img src="//pub.idqqimg.com/wpa/images/group.png" border="0" alt="QQ" /> </a> | qq开发群2:<a href="https://jq.qq.com/?_wv=1027&k=MCtjz6B9" target="_blank"><img src="//pub.idqqimg.com/wpa/images/group.png" border="0" alt="QQ" /> </a> | qq开发群3:<a href="https://jq.qq.com/?_wv=1027&k=H9FLIfTP" target="_blank"><img src="//pub.idqqimg.com/wpa/images/group.png" border="0" alt="QQ" /></a>

### 合作伙伴
![输入图片说明](https://images.gitee.com/uploads/images/2020/0725/120430_ab7fff0d_6569472.png "画板 1 拷贝 3(4).png")


### 版权信息

版权所有Copyright © 2015-2020 NiuShop开源商城&nbsp;版权所有

All rights reserved。
 
杭州牛之云科技有限公司&nbsp;提供技术支持  