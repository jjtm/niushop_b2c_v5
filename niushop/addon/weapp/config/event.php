<?php
// 事件定义文件
return [
    'bind' => [

    ],

    'listen' => [
        // 生成获取二维码
        'Qrcode' => [
            'addon\weapp\event\Qrcode'
        ],
        // 开放数据解密
        'DecryptData' => [
            'addon\weapp\event\DecryptData'
        ],
        // 获取手机号
        'PhoneNumber' => [
            'addon\weapp\event\PhoneNumber'
        ],
        // 发货成功
        'OrderDeliveryAfter' => [
            'addon\weapp\event\OrderDeliveryAfter'
        ],
        // 发货成功
        'OrderDeliveryAfterWeappDelivery' => [
            'addon\weapp\event\OrderDeliveryAfterWeappDelivery'
        ],
        // 订单收货后执行事件（异步）
        'OrderTakeDeliveryAfter' => [
            'addon\weapp\event\OrderTakeDeliveryAfter'
        ],
        // 礼品卡订单支付后
        'GiftCardOrderPay' => [
            'addon\weapp\event\GiftCardOrderPay'
        ],
        // 礼品卡订单支付后小程序发货
        'GiftCardOrderPayWeappDelivery' => [
            'addon\weapp\event\GiftCardOrderPayWeappDelivery'
        ],
        // 充值订单支付后
        'MemberRechargeOrderPay' => [
            'addon\weapp\event\MemberRechargeOrderPay'
        ],
        // 充值订单支付后小程序发货
        'MemberRechargeOrderPayWeappDelivery' => [
            'addon\weapp\event\MemberRechargeOrderPayWeappDelivery'
        ],
    ],

    'subscribe' => [
    ],
];
