<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 杭州牛之云科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace addon\weapp\event;

use app\model\system\Cron;

/**
 * 礼品卡订单支付后
 */
class GiftCardOrderPay
{
    public function handle($param)
    {
        //支付后立即调用发货接口，微信会提示订单不存在，所以延迟一分钟执行，如果是
        //{"errcode":10060001,"errmsg":"支付单不存在 rid: 66235dcf-4803e8cf-5c30a69e"}
        (new Cron())->addCron(1, 0, "礼品卡支付后小程序发货", "GiftCardOrderPayWeappDelivery", time() + 60, $param[ 'order_id' ]);
    }
}