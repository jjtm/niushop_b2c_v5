<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 杭州牛之云科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace addon\wechatpay\event;

use addon\wechatpay\model\Pay as PayModel;

/**
 * 支付回调
 */
class PayNotify
{
    /**
     * 支付方式及配置
     */
    public function handle($param)
    {

        try {
            $reqData = empty($GLOBALS[ 'HTTP_RAW_POST_DATA' ]) ? file_get_contents('php://input') : $GLOBALS[ 'HTTP_RAW_POST_DATA' ];
            if (empty($reqData)) {
                return;
            }
            $xmlstring = simplexml_load_string($reqData, 'SimpleXMLElement', LIBXML_NOCDATA);
            if (empty($xmlstring)) {
                return;
            }
            $val = json_decode(json_encode($xmlstring), true);
            if (!empty($val[ 'trade_type' ]) && $val[ 'trade_type' ] != 'APP') {
                return ( new PayModel() )->payNotify();
            }
        } catch (\Exception $e) {
            return ( new PayModel() )->payNotify();
        }

    }
}