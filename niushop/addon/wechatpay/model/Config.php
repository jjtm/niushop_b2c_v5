<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 杭州牛之云科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace addon\wechatpay\model;

use app\model\system\Config as ConfigModel;
use app\model\BaseModel;

/**
 * 微信支付配置
 * 版本 1.0.4
 */
class Config extends BaseModel
{

    private $encrypt = '******';

    /**
     * 设置支付配置
     * @param $data
     * @param int $site_id
     * @param string $app_module
     * @return array
     */
    public function setPayConfig($data, $site_id = 0, $app_module = 'shop')
    {
        $config = new ConfigModel();

        // 未加密前的数据
        $original_config = $this->getPayConfig($site_id)[ 'data' ][ 'value' ];

        // 检测数据是否发生变化，如果没有变化，则保持未加密前的数据
        if (!empty($data[ 'pay_signkey' ]) && $data[ 'pay_signkey' ] == $this->encrypt) {
            $data[ 'pay_signkey' ] = $original_config[ 'pay_signkey' ]; // APIv2密钥
        }
        if (!empty($data[ 'apiclient_cert' ]) && $data[ 'apiclient_cert' ] == $this->encrypt) {
            $data[ 'apiclient_cert' ] = $original_config[ 'apiclient_cert' ]; // 支付证书cert
        }
        if (!empty($data[ 'apiclient_key' ]) && $data[ 'apiclient_key' ] == $this->encrypt) {
            $data[ 'apiclient_key' ] = $original_config[ 'apiclient_key' ]; // 支付证书key
        }
        if (!empty($data[ 'plateform_cert' ]) && $data[ 'plateform_cert' ] == $this->encrypt) {
            $data[ 'plateform_cert' ] = $original_config[ 'plateform_cert' ]; // 平台证书
        }
        if (!empty($data[ 'v3_pay_signkey' ]) && $data[ 'v3_pay_signkey' ] == $this->encrypt) {
            $data[ 'v3_pay_signkey' ] = $original_config[ 'v3_pay_signkey' ]; // APIv3密钥
        }

        $res = $config->setConfig($data, '微信支付配置', 1, [ [ 'site_id', '=', $site_id ], [ 'app_module', '=', $app_module ], [ 'config_key', '=', 'WECHAT_PAY_CONFIG' ] ]);
        return $res;
    }

    /**
     * 获取支付配置
     * @param int $site_id
     * @param string $app_module
     * @param bool $need_encrypt 是否需要加密数据，true：加密、false：不加密
     * @return array
     */
    public function getPayConfig($site_id = 0, $app_module = 'shop', $need_encrypt = false)
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', $site_id ], [ 'app_module', '=', $app_module ], [ 'config_key', '=', 'WECHAT_PAY_CONFIG' ] ]);
        if (!empty($res[ 'data' ][ 'value' ])) {
            $res[ 'data' ][ 'value' ][ 'api_type' ] = $res[ 'data' ][ 'value' ][ 'api_type' ] ?? 'v2';
            $res[ 'data' ][ 'value' ][ 'transfer_type' ] = $res[ 'data' ][ 'value' ][ 'transfer_type' ] ?? 'v2';
        }
        if (!empty($res[ 'data' ][ 'value' ]) && $need_encrypt) {
            // 加密敏感信息
            if (!empty($res[ 'data' ][ 'value' ][ 'pay_signkey' ])) {
                $res[ 'data' ][ 'value' ][ 'pay_signkey' ] = $this->encrypt; // APIv2密钥
            }

            if (!empty($res[ 'data' ][ 'value' ][ 'apiclient_cert' ])) {
                $res[ 'data' ][ 'value' ][ 'apiclient_cert' ] = $this->encrypt; // 支付证书cert
            }

            if (!empty($res[ 'data' ][ 'value' ][ 'apiclient_key' ])) {
                $res[ 'data' ][ 'value' ][ 'apiclient_key' ] = $this->encrypt; // 支付证书key
            }

            if (!empty($res[ 'data' ][ 'value' ][ 'plateform_cert' ])) {
                $res[ 'data' ][ 'value' ][ 'plateform_cert' ] = $this->encrypt; // 平台证书
            }

            if (!empty($res[ 'data' ][ 'value' ][ 'v3_pay_signkey' ])) {
                $res[ 'data' ][ 'value' ][ 'v3_pay_signkey' ] = $this->encrypt; // APIv3密钥
            }
        }
        return $res;
    }
}