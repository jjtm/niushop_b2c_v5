<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 杭州牛之云科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace app\shop\controller;

use addon\weapp\model\Config as WeappConfigModel;
use app\Controller;
use app\model\store\Store as StoreModel;
use app\model\system\Addon;
use app\model\system\Group as GroupModel;
use app\model\system\Menu;
use app\model\system\Site;
use app\model\system\User as UserModel;
use app\model\upload\Config as UploadConfigModel;
use app\model\web\Config as ConfigModel;
use app\model\web\DiyView as DiyViewModel;
use think\App;
use think\facade\Config;

class BaseShop extends Controller
{

    protected $uid;
    protected $user_info;
    protected $url;
    protected $group_info;
    protected $site_id;
    protected $store_id;
    protected $shop_info;
    protected $store_info;
    protected $app_module = SHOP_MODULE;
    protected $replace = [];
    protected $addon = '';

    /**
     * 模板空布局文件，初始化数据
     * @var string|bool
     */
    protected $layout = 'app/shop/view/layout/base.html';

    // 模版布局文件，加载菜单+内容，default：默认模板，后续支持扩展
    protected $template = 'app/shop/view/layout/default.html';


    public function __construct(App $app = null)
    {
        //执行父类构造函数
        parent::__construct();

        $this->app = $app;

        //检测基础登录
        $this->site_id = request()->siteid();

        $user_model = new UserModel();
        $this->app_module = $user_model->loginModule($this->site_id);
        $this->uid = $user_model->uid($this->app_module, $this->site_id);

        // 验证登录
        if (empty($this->uid)) {
            $this->redirect(url('shop/login/login'));
        }

        $this->url = request()->parseUrl();
        $this->addon = request()->addon() ? request()->addon() : '';
        $this->user_info = $user_model->userInfo($this->app_module, $this->site_id);

        $this->assign('user_info', $this->user_info);
        $this->assign('app_module', $this->app_module);

        // 检测用户组
        $this->getGroupInfo();

        if ($this->app_module == 'store') {
            //检测用户组,通过用户组查询对应门店id
            $store_model = new StoreModel();
            $this->store_info = $store_model->getStoreInfo([ [ 'store_id', '=', $this->store_id ] ])[ 'data' ];
            if ($this->store_info[ 'is_frozen' ]) {
                $this->error('该门店已关闭，请联系店铺管理员开启');
            }
        }

        if (!$this->checkAuth()) {
            if (request()->isJson()) {
                $menu_model = new Menu();
                $menu_info = $menu_model->getMenuInfoByUrl($this->url, $this->app_module, $this->addon)['data'];
                if (!empty($menu_info) && $menu_info['is_control'] == 1) {
                    echo json_encode(error(-1, '权限不足，请联系管理员', $menu_info));
                    exit;
                }
            } elseif (request()->isAjax() && request()->type() == 'html') {
                $menu_info = $user_model->getRedirectUrl($this->url, $this->app_module, $this->group_info, $this->addon);
                if (empty($menu_info)) {
                    $this->error('权限不足，请联系管理员');
                } elseif (!empty($menu_info[ 'url' ])) {
                    // 当前页面没有权限，跳转同级别功能页面
                    $this->redirect(addon_url($menu_info[ 'url' ]));
                }
            }
        }

        //获取店铺信息
        $site_model = new Site();
        $this->shop_info = $site_model->getSiteInfo([ [ 'site_id', '=', $this->site_id ] ], 'site_id,site_name,logo,seo_keywords,seo_description, create_time')[ 'data' ];
        $this->assign('shop_info', $this->shop_info);

        // 加载自定义图标库
        $diy_view = new DiyViewModel();
        $diy_icon_url = $diy_view->getIconUrl()[ 'data' ];
        $this->assign('load_diy_icon_url', $diy_icon_url);

        // 上传图片配置
        $uplode_config_model = new UploadConfigModel();
        $upload_config = $uplode_config_model->getUploadConfig($this->site_id);
        $this->assign('upload_max_filesize', $upload_config[ 'data' ][ 'value' ][ 'upload' ][ 'max_filesize' ] / 1024);

        // 后台主题风格
        $config_model = new ConfigModel();
        $theme_config = $config_model->getThemeConfig()[ 'data' ][ 'value' ];
        $this->assign('theme_config', $theme_config);

        // 在设置模板布局之前，设置变量输出
        $config_view = Config::get('view');
        $config_view[ 'tpl_replace_string' ] = array_merge($config_view[ 'tpl_replace_string' ], $this->replace);
        Config::set($config_view, 'view');

        if (!request()->isAjax()) {
            $this->initBaseInfo();
            $this->loadTemplate();
        }

    }

    /**
     * 加载基础信息
     */
    private function initBaseInfo()
    {

        $this->assign('url', $this->url);

        //加载版权信息
        $config_model = new ConfigModel();
        $copyright = $config_model->getCopyright();
        $this->assign('copyright', $copyright[ 'data' ][ 'value' ]);

        // 查询小程序配置信息
        if (addon_is_exit('weapp', $this->site_id)) {
            $weapp_config_model = new WeappConfigModel();
            $weapp_config = $weapp_config_model->getWeappConfig($this->site_id)[ 'data' ][ 'value' ];
            $this->assign('base_weapp_config', $weapp_config);
        }
    }

    /**
     * 获取当前用户的用户组
     */
    private function getGroupInfo()
    {
        $group_model = new GroupModel();
        $group_info_result = $group_model->getGroupInfo([ [ 'group_id', '=', $this->user_info[ 'group_id' ] ], [ 'app_module', '=', $this->app_module ] ]);
        $this->group_info = $group_info_result[ 'data' ];
        if ($this->app_module == 'store') {
            //门店登录,用户权限对应站点id是门店id
            $this->store_id = $this->group_info[ 'site_id' ];
        }
    }

    /**
     * 检测权限
     */
    private function checkAuth()
    {
        if ($this->user_info[ 'is_admin' ] == 1) {
            return true;
        }
        $user_model = new UserModel();
        $auth = $user_model->checkAuth($this->url, $this->app_module, $this->group_info, $this->addon);
        return $auth[ 'code' ] == 0;
    }

    /**
     * 获取菜单
     */
    private function getMenuList()
    {
        $field = 'id, app_module, addon, title, name, parent, level, url, is_show, sort, picture, picture_select';
        $menu_model = new Menu();
        //暂定全部权限，系统用户做完后放开
        if ($this->user_info[ 'is_admin' ] || $this->group_info[ 'is_system' ] == 1) {
            $menus = $menu_model->getMenuList([ [ 'app_module', '=', $this->app_module ] ], $field, 'level asc,sort asc');
        } else {
            $menus = $menu_model->getMenuList([ [ 'name', 'in', $this->group_info[ 'menu_array' ] ], [ 'app_module', '=', $this->app_module ] ], $field, 'level asc,sort asc');
            $control_menu = $menu_model->getMenuList([ [ 'is_control', '=', 0 ], [ 'app_module', '=', $this->app_module ] ], $field, 'sort asc');
            $menus[ 'data' ] = array_merge($control_menu[ 'data' ], $menus[ 'data' ]);
            $keys = array_column($menus[ 'data' ], 'sort');
            if (!empty($keys)) {
                array_multisort($keys, SORT_ASC, SORT_NUMERIC, $menus[ 'data' ]);
            }
        }

        return $menus[ 'data' ];
    }

    /**
     * 添加日志
     * @param $action_name
     * @param array $data
     */
    protected function addLog($action_name, $data = [])
    {
        $user = new UserModel();
        $user->addUserLog($this->uid, $this->user_info[ 'username' ], $this->site_id, $action_name, $data);
    }

    public function __call($method, $args)
    {
        return $this->fetch(app()->getRootPath() . 'public/error/error.html');
    }

    /**
     * 加载模板页面
     */
    public function loadTemplate()
    {
        if (!request()->isAjax()) {

            // 设置模版布局
            $this->app->view->engine()->layout($this->layout);

            // 请求方式，空：返回菜单+内容，iframe：iframe标签模式，返回内容，download：返回下载资源
            $request_mode = input('request_mode', '');
            if (empty($request_mode)) {
                echo $this->fetch($this->template);
                exit;
            }

        }
    }

    /**
     * 查询菜单列表
     * @return array
     */
    public function menu()
    {
        if (request()->isJson()) {
            $url = input('url', $this->url);

            $res = [
                'url' => $url
            ];

            $menus = $this->getMenuList();

            $addon_model = new Addon();
            $res[ 'quick_addon_menu' ] = $addon_model->getAddonQuickMenuConfig($this->site_id, $this->app_module)[ 'data' ][ 'value' ];

            $user_model = new UserModel();

            if ($this->user_info[ 'is_admin' ] == 0 && $this->group_info[ 'is_system' ] == 0) {

                $menu_model = new Menu();

                // 查询有权限的菜单列表
                $menu_list = $menu_model->getMenuList([
                    [ 'is_show', '=', 1 ],
                    [ 'name', 'in', $this->group_info[ 'menu_array' ] ],
                    [ 'app_module', '=', $this->app_module ]
                ],
                    'id, app_module, name, title, parent, level, url, is_show, sort, `desc`, picture, is_icon, picture_select, is_control, addon',
                    'level desc,sort asc')[ 'data' ];
                $menu_list = array_column($menu_list, null, 'name');

                foreach ($menus as $k => $v) {

                    // 将1，2，3级菜单和营销活动菜单的链接改成有权限的路径
                    if (in_array($v[ 'level' ], [ 1, 2, 3 ]) || in_array($v[ 'parent' ], [ 'PROMOTION_CENTER', 'PROMOTION_TOOL' ])) {
                        $auth = $user_model->checkAuth($v[ 'url' ], $this->app_module, $this->group_info, $this->addon);
                        if (( $auth[ 'code' ] ) < 0 && !empty($auth[ 'data' ])) {
                            $redirect_url = $user_model->getAuthUrl($auth[ 'data' ], $this->app_module, $this->group_info, $menu_list);
                            if (!empty($redirect_url)) {
                                $menus[ $k ][ 'url' ] = $redirect_url[ 'url' ];
                            }
                        }
                    }

                }
            }

            $menu_info = $user_model->getRedirectUrl($url, $this->app_module, $this->group_info, $this->addon);
            if (!empty($menu_info)) {
                $res[ 'url' ] = $menu_info[ 'url' ];
            }

            $res[ 'yuan_menu' ] = $menus;

            return $res;
        }
    }

}