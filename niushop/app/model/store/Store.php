<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 杭州牛之云科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * =========================================================
 */

namespace app\model\store;

use app\model\BaseModel;
use app\model\express\Config;
use think\facade\Cache;
use think\facade\Db;
use app\model\upload\Upload;

/**
 * 门店管理
 */
class Store extends BaseModel
{
    //门店基础信息做缓存处理，门店业务信息单独查询处理
    public $cache_model = 'cache_model_store';

    /**
     * 添加门店
     * @param $data
     * @param array $user_data
     * @param int $is_store
     * @return array
     */
    public function addStore($data, $user_data = [], $is_store = 0)
    {

        $site_id = $data[ 'site_id' ] ?? '';
        if ($site_id === '') {
            return $this->error('', 'REQUEST_SITE_ID');
        }
        if (empty($data[ 'longitude' ]) || empty($data[ 'latitude' ])) {
            return $this->error('', '门店经纬度不能为空');
        }

        $data[ 'create_time' ] = time();
        model('store')->startTrans();

        try {
            $store_id = model('store')->add($data);
            if ($is_store == 1 && isset($user_data[ 'uid' ]) && !empty($user_data[ 'uid' ])) {
                // 添加门店管理员
                $group_id = model('cashier_auth_group')->getValue([ [ 'site_id', '=', $site_id ], [ 'keyword', '=', 'admin' ] ], 'group_id');
                model('user_group')->add([
                    'uid' => $user_data[ 'uid' ],
                    'site_id' => $site_id,
                    'store_id' => $store_id,
                    'group_id' => $group_id,
                    'create_time' => time(),
                    'app_module' => 'store'
                ]);
            }
            //执行事件
            event("AddStore", [ 'store_id' => $store_id, 'site_id' => $data[ 'site_id' ] ]);

            model('store')->commit();
            Cache::tag($this->cache_model)->clear();
            return $this->success($store_id);
        } catch (\Exception $e) {
            model('store')->rollback();
            return $this->error('', $e->getMessage());
        }

    }

    /**
     * 修改门店
     * @param $data
     * @param $condition
     * @param array $user_data
     * @param int $is_exit
     * @param int $user_type
     * @return array
     */
    public function editStore($data, $condition, $user_data = [], $is_exit = 0, $user_type = 1)
    {
        if (( isset($data[ 'longitude' ]) && empty($data[ 'longitude' ]) ) || ( isset($data[ 'latitude' ]) && empty($data[ 'latitude' ]) )) {
            return $this->error('', '门店经纬度不能为空');
        }
        $check_condition = array_column($condition, 2, 0);
        $site_id = $check_condition[ 'site_id' ] ?? '';
        $store_id = $check_condition[ 'store_id' ] ?? '';
        if ($site_id === '') {
            return $this->error('', 'REQUEST_SITE_ID');
        }
        $data[ "modify_time" ] = time();

        model('store')->startTrans();
        try {
            $store_info = model('store')->getInfo($condition);
            if ($store_info[ 'store_image' ] && !empty($data[ 'store_image' ]) && $store_info[ 'store_image' ] != $data[ 'store_image' ]) {
                $upload_model = new Upload();
                $upload_model->deletePic($store_info[ 'store_image' ], $site_id);
            }

            model('store')->update($data, $condition);

            //可能会关闭门店自提方式
            $this->checkCloseStoreTrade($site_id);

            model('store')->commit();
            Cache::tag($this->cache_model)->clear();
            return $this->success($store_id);
        } catch (\Exception $e) {
            model('store')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 删除门店
     * @param array $condition
     */
    public function deleteStore($condition)
    {
        $check_condition = array_column($condition, 2, 0);
        $site_id = $check_condition[ 'site_id' ] ?? '';
        $store_id = $check_condition[ 'store_id' ] ?? '';
        if ($site_id === '') {
            return $this->error('', 'REQUEST_SITE_ID');
        }

        $store_info = model('store')->getInfo([ [ 'store_id', '=', $store_id ] ], 'uid, store_image');
        if (!empty($store_info[ 'store_image' ])) {
            $upload_model = new Upload();
            $upload_model->deletePic($store_info[ 'store_image' ], $site_id);
        }
        $res = model('store')->delete($condition);
        if ($res) {
            model('store_goods')->delete([ [ 'store_id', '=', $store_id ] ]);
            model('store_goods_sku')->delete([ [ 'store_id', '=', $store_id ] ]);
            model('store_member')->delete([ [ 'store_id', '=', $store_id ] ]);
            model('store_settlement')->delete([ [ 'store_id', '=', $store_id ], [ 'site_id', '=', $site_id ] ]);
            model('user')->delete([ [ 'app_module', '=', 'store' ], [ 'site_id', '=', $site_id ], [ 'uid', '=', $store_info[ 'uid' ] ] ]);
            model('site_diy_view')->delete([ [ 'name', '=', 'DIY_STORE_' . $store_id ], [ 'site_id', '=', $site_id ] ]);
        }
        //可能会关闭门店自提方式
        $this->checkCloseStoreTrade($site_id);
        Cache::tag($this->cache_model)->clear();
        return $this->success($res);
    }

    /**
     * 获取门店数量
     * @param $where
     * @param $field
     * @return array
     */
    public function getStoreCount($where, $field = 'store_id')
    {
        $cache_name = $this->cache_model . '_' . __FUNCTION__ . '_' . serialize(func_get_args());
        $cache = Cache::get($cache_name, "");
        if (!empty($cache)) {
            return $this->success($cache);
        }
        $res = model('store')->getCount($where, $field);
        Cache::tag($this->cache_model)->set($cache_name, $res);
        return $this->success($res);
    }

    /**
     * 获取门店字段和
     * @param $where
     * @param $field
     * @return array
     */
    public function getStoreSum($where, $field)
    {
        $res = model('store')->getSum($where, $field);
        return $this->success($res);
    }

    /**
     * @param $condition
     * @param $is_frozen
     * @return array
     */
    public function frozenStore($condition, $is_frozen)
    {
        $check_condition = array_column($condition, 2, 0);
        $site_id = $check_condition[ 'site_id' ] ?? '';
        if ($site_id === '') {
            return $this->error('', 'REQUEST_SITE_ID');
        }
        $res = model('store')->update([ 'is_frozen' => $is_frozen == 1 ? 0 : 1 ], $condition);
        //可能会关闭门店自提方式
        $this->checkCloseStoreTrade($site_id);
        Cache::tag($this->cache_model)->clear();
        return $this->success($res);
    }

    /**
     * 重置密码
     * @param string $password
     * @param $condition
     * @return array
     */
    public function resetStorePassword($password = '123456', $condition = [])
    {
        //获取用户id
        $uid = model('store')->getValue($condition, 'uid');
        if ($uid) {
            $res = model('user')->update([
                'password' => data_md5($password)
            ], [ [ 'uid', '=', $uid ] ]);
        } else {
            $res = 1;
        }
        if ($res === false) {
            return $this->error('', 'RESULT_ERROR');
        }
        return $this->success($res);
    }

    /**
     * 获取门店信息(不包含门店账户)(缓存信息)
     * @param $condition
     * @param string $field
     * @return array
     */
    public function getStoreInfo($condition, $field = '*')
    {
        $cache_name = $this->cache_model . '_' . __FUNCTION__ . '_' . serialize(func_get_args());
        $res = Cache::get($cache_name, "");
        if (empty($res)) {
            $res = model('store')->getInfo($condition, $field);
            $res = $this->dealWithStoreDeliveryTime($res);
            Cache::tag($this->cache_model)->set($cache_name, $res);
        }
        return $this->success($res);
    }

    /**
     * 获取门店信息(包含门店账户动态查询)(无缓存)
     * @param $condition
     * @param string $field
     * @return array
     */
    public function getStoreInfoByAccount($condition, $field = '*')
    {
        $res = model('store')->getInfo($condition, $field);
        $res = $this->dealWithStoreDeliveryTime($res);
        if (!empty($res)) {
            if (!empty($res[ 'time_week' ])) {
                $res[ 'time_week' ] = explode(',', $res[ 'time_week' ]);
            }
            if (empty($res[ 'delivery_time' ])) {
                $temp_delivery_time = json_decode($res[ 'delivery_time' ], true);
                if(empty($temp_delivery_time)){
                    $res[ 'delivery_time' ] = [
                        [ 'start_time' => $res[ 'start_time' ], 'end_time' => $res[ 'end_time' ] ]
                    ];
                }

            } else {
                $res[ 'delivery_time' ] = json_decode($res[ 'delivery_time' ], true);
            }
        }
        return $this->success($res);
    }

    /**
     * 处理配送时间数据
     * TODO 配送时间特殊处理，保存为[]会导致前端报错但是还不知道错误是怎么发生的
     * TODO 前端需要处理兼容并且找到问题的根源
     * @param $info
     * @return mixed
     */
    protected function dealWithStoreDeliveryTime($info)
    {
        if(isset($info['delivery_time']) && $info['delivery_time'] == '[]'){
            $info['delivery_time'] = '';
        }
        return $info;
    }

    /**
     * 获取门店详情(不包含门店账户)(缓存)
     * @param $condition
     * @param string $field
     * @return array
     */
    public function getStoreDetail($condition, $field = '*')
    {
        $res = model('store')->getInfo($condition, $field);
        $res = $this->dealWithStoreDeliveryTime($res);
        if (!empty($res)) {
            if (!empty($res[ 'time_week' ])) {
                $res[ 'time_week' ] = explode(',', $res[ 'time_week' ]);
            }
            if (empty($res[ 'delivery_time' ])) {
                $res[ 'delivery_time' ] = [
                    [ 'start_time' => $res[ 'start_time' ], 'end_time' => $res[ 'end_time' ] ]
                ];
            } else {
                $res[ 'delivery_time' ] = json_decode($res[ 'delivery_time' ], true);
            }
        }
        return $this->success($res);
    }

    /**
     * 获取门店列表(缓存信息)
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param null $limit
     * @return array
     */
    public function getStoreList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $cache_name = $this->cache_model . '_' . __FUNCTION__ . '_' . serialize(func_get_args());
        $cache = Cache::get($cache_name, "");
        if (!empty($cache)) {
            return $this->success($cache);
        }
        $list = model('store')->getList($condition, $field, $order, '', '', '', $limit);
        Cache::tag($this->cache_model)->set($cache_name, $list);
        return $this->success($list);
    }

    /**
     * 获取门店分页列表(缓存信息)
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getStorePageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $cache_name = $this->cache_model . '_' . __FUNCTION__ . '_' . serialize(func_get_args());
        $cache = Cache::get($cache_name, "");
        if (!empty($cache)) {
            return $this->success($cache);
        }
        $list = model('store')->pageList($condition, $field, $order, $page, $page_size);
        Cache::tag($this->cache_model)->set($cache_name, $list);
        return $this->success($list);
    }

    /**
     * 获取门店分页列表(门店列表包含账户业务)(无缓存)
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getStorePageListByAccount($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('store')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }

    /**
     * 获取门店名称(鉴于调用场景过多,封装一个只返回门店名称的函数,todo  做缓存)
     * @param $condition
     * @return array
     */
    public function getStoreName($condition)
    {
        $cache_name = $this->cache_model . '_' . __FUNCTION__ . '_' . serialize(func_get_args());
        $cache = Cache::get($cache_name, "");
        if (!empty($cache)) {
            return $this->success($cache);
        }
        $name = model('store')->getValue($condition, 'store_name');
        Cache::tag($this->cache_model)->set($cache_name, $name);
        return $this->success($name);
    }

    /**
     * 获取默认门店（缓存)
     * @param int $site_id 只有单商户这么写
     * @param string $field
     * @return array
     */
    public function getDefaultStore($site_id = 0, $field = '*')
    {
        $condition = array (
            [ 'is_default', '=', 1 ]
        );
        if ($site_id > 0) {
            $condition[] = [ 'site_id', '=', $site_id ];
        }
        $cache_name = $this->cache_model . '_' . __FUNCTION__ . '_' . serialize(func_get_args());
        $info = Cache::get($cache_name, "");
        if (empty($info)) {
            $info = model('store')->getInfo($condition, $field);
            $info = $this->dealWithStoreDeliveryTime($info);
            Cache::tag($this->cache_model)->set($cache_name, $info);
        }
        return $this->success($info);
    }

    /**
     * 填写店铺默认门店
     * @param $params
     * @return array
     */
    public function addDefaultStore($params)
    {
        $site_id = $params[ 'site_id' ] ?? 1;
        $data = array (
            'site_id' => $site_id,
            'store_name' => '默认门店',
            'is_default' => 1,
            'create_time' => time()
        );
        $res = model('store')->add($data);
        Cache::tag($this->cache_model)->clear();
        return $this->success();
    }

    /**
     * 获取扣除库存门店
     * @param $params
     * @return array
     */
    public function getStoreStockTypeStoreId($params)
    {
        $store_id = $params[ 'store_id' ];
        $store_condition = array (
            [ 'store_id', '=', $store_id ]
        );
        $store_info = $this->getStoreInfo($store_condition)[ 'data' ] ?? [];
        if (empty($store_info)) {
            return $this->error();
        }
        $stock_type = $store_info[ 'stock_type' ];

        if ($stock_type == 'all') {
            $default_store_info = $this->getDefaultStore()[ 'data' ] ?? [];
            $store_id = $default_store_info[ 'store_id' ];
        }
        return $this->success($store_id);
    }


    /**
     * 获取门店类型
     * @param string $type
     * @return array
     */
    public function getStoreType($type = '')
    {
        $store_type = [
            'directsale' => [
                'type' => 'directsale',
                'name' => '直营店'
            ],
            'franchise' => [
                'type' => 'franchise',
                'name' => '加盟店'
            ]
        ];
        return $type ? $store_type[ $type ] : $store_type;
    }

    /**
     * 查询门店  带有距离
     * @param $condition
     * @param $field
     * @param $lnglat
     * @param null $limit
     * @return array
     */
    public function getLocationStoreList($condition, $field, $lnglat, $limit = null)
    {
        try {
            $order = '';
            if (!empty($lnglat[ 'lat' ]) && !empty($lnglat[ 'lng' ])) {
                $lnglat[ 'lat' ] = paramFilter($lnglat[ 'lat' ]);
                $lnglat[ 'lng' ] = paramFilter($lnglat[ 'lng' ]);
                $field .= ' , ROUND(st_distance ( point ( ' . $lnglat[ 'lng' ] . ', ' . $lnglat[ 'lat' ] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) as distance ';
                $condition[] = [ '', 'exp', Db::raw(' FORMAT(st_distance ( point ( ' . $lnglat[ 'lng' ] . ', ' . $lnglat[ 'lat' ] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) < 10000') ];
                $order = 'distance asc';
            }
            $list = model('store')->getList($condition, $field, $order, '', '', '', $limit);
            return $this->success($list);
        } catch (\Exception $e) {
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 查询门店  带有距离
     * @param $condition
     * @param $page
     * @param $page_size
     * @param $field
     * @param $lnglat
     * @return array
     */
    public function getLocationStorePageList($condition, $page, $page_size, $field, $lnglat)
    {
        $order = '';
        if (!empty($lnglat[ 'lat' ]) && !empty($lnglat[ 'lng' ])) {
            $lnglat[ 'lat' ] = paramFilter($lnglat[ 'lat' ]);
            $lnglat[ 'lng' ] = paramFilter($lnglat[ 'lng' ]);
            $field .= ',FORMAT(st_distance ( point ( ' . $lnglat[ 'lng' ] . ', ' . $lnglat[ 'lat' ] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) as distance';
            $condition[] = [ '', 'exp', Db::raw(' FORMAT(st_distance ( point ( ' . $lnglat[ 'lng' ] . ', ' . $lnglat[ 'lat' ] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) < 10000') ];
            $order = Db::raw(' st_distance ( point ( ' . $lnglat[ 'lng' ] . ', ' . $lnglat[ 'lat' ] . ' ), point ( longitude, latitude ) ) * 111195 / 1000 asc');
        }
        $list = model('store')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }

    /**
     * 核验是否可以关闭门店自提
     * @param $site_id
     * @return array
     */
    public function checkCloseStoreTrade($site_id)
    {
        $count = model('store')->getCount([ [ 'site_id', '=', $site_id ], [ 'is_pickup', '=', 1 ], [ 'status', '=', 1 ], [ 'is_frozen', '=', 0 ] ]);
        if ($count == 0) {
            //站点的所有门店都被删除后,门店开关也会被关闭
            $config_model = new Config();
            $config_model->setStoreIsuse(0, $site_id);
        }
        return $this->success();
    }

    /**
     * 核验是否可以开启门店自提
     * @param $site_id
     * @return array
     */
    public function checkIscanStoreTrade($site_id)
    {
        $count = model('store')->getCount([ [ 'site_id', '=', $site_id ], [ 'is_pickup', '=', 1 ], [ 'status', '=', 1 ], [ 'is_frozen', '=', 0 ] ]);
        if ($count == 0) {
            return $this->error('', '需至少存在一个营业中且开启自提业务的门店,才能开启门店自提开关');
        } else {
            return $this->success();
        }
    }
}